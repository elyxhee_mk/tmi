/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 5.6.32-78.1 : Database - demoappo_tmi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`demoappo_tmi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `demoappo_tmi`;

/*Table structure for table `contents` */

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('audio','video','text','photo') COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_type` enum('course','module','task') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_category` enum('Anchor','Journalist') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_content` text COLLATE utf8mb4_unicode_ci,
  `is_report` int(1) DEFAULT NULL,
  `is_blocked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contents` */

insert  into `contents`(`id`,`user_id`,`title`,`path`,`original_name`,`type`,`file_type`,`description`,`content_type`,`course_category`,`task_content`,`is_report`,`is_blocked`,`parent_id`,`created_at`,`updated_at`,`deleted_at`) values 
(29,20,'Course 1','RkPkXXSOYGdGUILezzuWyZX20Fgrd2uXhLePsb2X.jpeg','Lighthouse.jpg','photo','image/jpeg','test course','course','Anchor',NULL,NULL,0,0,'2018-02-20 09:29:32','2018-02-20 09:29:32',NULL),
(30,20,'Module 1','NULL','NULL','text','NULL','test module','module',NULL,NULL,NULL,0,29,'2018-02-20 09:36:43','2018-02-20 09:36:43',NULL),
(31,20,'task 1','NULL','NULL','text','NULL','test task','task',NULL,'this  a task content which has to display on screen',NULL,0,30,'2018-02-20 09:38:47','2018-02-20 09:38:47',NULL),
(32,20,'task 1','NULL','NULL','text','NULL','test task','task',NULL,'this  a task content which has to display on screen',NULL,0,30,'2018-02-20 09:45:58','2018-02-20 09:45:58',NULL),
(33,20,'task 1','NULL','NULL','audio','NULL','test task','task',NULL,'this  a task content which has to display on screen',NULL,0,30,'2018-02-20 09:51:05','2018-02-20 09:51:05',NULL),
(34,20,'course 2','FA3uNEfMLWb8FbVYwIt9Ad5ZOQKqJtlqBAYqRkVA.jpeg','Lighthouse.jpg','photo','image/jpeg','test task','course','Anchor',NULL,NULL,0,0,'2018-02-20 09:52:23','2018-02-20 09:52:23',NULL);

/*Table structure for table `device_token` */

DROP TABLE IF EXISTS `device_token`;

CREATE TABLE `device_token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `udid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('ios','android') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ios',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `device_token` */

/*Table structure for table `task_content` */

DROP TABLE IF EXISTS `task_content`;

CREATE TABLE `task_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `task_content` */

/*Table structure for table `user_enrollments` */

DROP TABLE IF EXISTS `user_enrollments`;

CREATE TABLE `user_enrollments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `user_enrollments` */

insert  into `user_enrollments`(`id`,`user_id`,`course_id`,`created_at`,`updated_at`,`deleted_at`) values 
(16,20,29,'2018-02-20 09:55:34','2018-02-20 09:55:34',NULL);

/*Table structure for table `user_profile_actions` */

DROP TABLE IF EXISTS `user_profile_actions`;

CREATE TABLE `user_profile_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `type` enum('view','report') DEFAULT 'view',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `user_profile_actions` */

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `social_network` enum('facebook') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'facebook',
  `social_network_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_social_accounts_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_social_accounts` */

/*Table structure for table `user_task_contents` */

DROP TABLE IF EXISTS `user_task_contents`;

CREATE TABLE `user_task_contents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('audio','video','text') COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feedback_requested` int(1) DEFAULT NULL,
  `is_report` int(1) DEFAULT NULL,
  `is_blocked` tinyint(3) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_task_contents` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `subscription` enum('Monthly','Weekly') DEFAULT NULL,
  `preference_cat` enum('Any') DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `zip_code` int(6) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `visibility` int(1) DEFAULT '1',
  `gender` enum('male','female') DEFAULT NULL,
  `user_type` enum('Individual','Business') DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `business_established_at` datetime DEFAULT NULL,
  `signup_via` enum('email','facebook') DEFAULT NULL,
  `sponser` text,
  `package` varchar(255) DEFAULT NULL,
  `radius` varchar(16) DEFAULT '5' COMMENT 'Miles',
  `lattitude` varchar(255) DEFAULT NULL,
  `dream_collage` text,
  `longitude` varchar(255) DEFAULT NULL,
  `account_visibility` enum('public','private') DEFAULT NULL,
  `access_token` text,
  `login_at` datetime DEFAULT NULL,
  `recover_password_key` text,
  `recover_attempt_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`subscription`,`preference_cat`,`age`,`zip_code`,`dob`,`image_path`,`profile_pic`,`visibility`,`gender`,`user_type`,`business_name`,`business_established_at`,`signup_via`,`sponser`,`package`,`radius`,`lattitude`,`dream_collage`,`longitude`,`account_visibility`,`access_token`,`login_at`,`recover_password_key`,`recover_attempt_at`,`created_at`,`updated_at`,`deleted_at`) values 
(19,'Mehwish','khan','elyxheedfb@gmail.com',NULL,'$2y$10$LkdbLnHoTOXkJx7pVQFjsO0s0Fzr2Iz0vquvP8LbQZWUurmpZAG7G','Monthly','Any',NULL,NULL,NULL,NULL,'JmGfvmHVTDgAOESXugJLBUQGixOgsxWJaKgkRpem.jpeg',1,NULL,'Individual',NULL,NULL,'email',NULL,NULL,'5',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE5LCJpc3MiOiJodHRwOi8vZGVtby1hcHBvY3RhLmNvbS9hcHB1bml0d29yay9UTUkvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTUxOTExNjUyOCwiZXhwIjoxNTIxNzQ0NTI4LCJuYmYiOjE1MTkxMTY1MjgsImp0aSI6IllmRDJBVjFMNXUzSWVIc0IifQ.kgMYIMeKgfEenri69P0WwmBN2PQeIJIdeCNXBKRLSuE','2018-02-20 08:48:48','rX1doGfO5bJE5BG2kdS1LU7hWBEd2siwyjF76ZF13dEU7XLSWqqym25rWDAk','2018-03-08 08:39:18','2018-02-20 08:48:48','2018-03-08 08:39:18',NULL),
(20,'Mehwish','khan','test124@test.com',NULL,'$2y$10$IZ.TjAUN/53gV1z7cVjD3ufW3gFKohbijeSrpgFfnxhkg5OW3Xg.y','Monthly','Any',NULL,NULL,NULL,NULL,'HorCB0kpp6s3ZoE2AVg6tlvwSqTLI0MelPxarVFk.jpeg',1,NULL,'Individual',NULL,NULL,'email',NULL,NULL,'5',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwLCJpc3MiOiJodHRwOi8vZGVtby1hcHBvY3RhLmNvbS9hcHB1bml0d29yay9UTUkvcHVibGljL2FwaS9sb2dpbiIsImlhdCI6MTUyMTYzODExNCwiZXhwIjoxNTI0MjY2MTE0LCJuYmYiOjE1MjE2MzgxMTQsImp0aSI6ImoyYTU5UkdZZXgySm91QW0ifQ.1SI3Y0tf98VzmnZafuJVhelFIUi-cPK6aoTIvE_yIR0','2018-03-21 13:15:14',NULL,NULL,'2018-02-20 09:21:03','2018-03-21 13:15:14',NULL),
(21,'danial','khan','danial@test.com',NULL,'$2y$10$bNffn1DmNxrzytIf8K2FX.8beeEyu7LiAYjAtJpHQRC/qTSjJavca','Monthly','Any',NULL,NULL,NULL,NULL,'nzYQahBkA3do30zoI7RVjQFufwqpVZmblFeQmfTl.jpeg',1,NULL,'Individual',NULL,NULL,'email',NULL,NULL,'5',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIxLCJpc3MiOiJodHRwOi8vZGVtby1hcHBvY3RhLmNvbS9hcHB1bml0d29yay9UTUkvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTUyMTU0NDIxOSwiZXhwIjoxNTI0MTcyMjE5LCJuYmYiOjE1MjE1NDQyMTksImp0aSI6InVJQW1TQzBUVjZNNU9WMDgifQ.O4kGgKYC-U-kFyvD1ENwu6ZWyiNXQm61XX9T2A60Uao','2018-03-20 11:10:19',NULL,NULL,'2018-03-20 11:10:19','2018-03-20 11:10:19',NULL),
(22,'danial','khan','ali@test.com',NULL,'$2y$10$IItGg5HoKqyCVG0LOgqq../CREEM7BDZcQFJRtQuNdioA9Fj6mzZi','Monthly','Any',NULL,NULL,NULL,NULL,'TFPVkVgj8uXDhCTvf0u6wNbsHL91tiMCN5L6YwxM.jpeg',1,NULL,'Individual',NULL,NULL,'email',NULL,NULL,'5',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyLCJpc3MiOiJodHRwOi8vZGVtby1hcHBvY3RhLmNvbS9hcHB1bml0d29yay9UTUkvcHVibGljL2FwaS9yZWdpc3RlciIsImlhdCI6MTUyMTYzMTQ0NiwiZXhwIjoxNTI0MjU5NDQ2LCJuYmYiOjE1MjE2MzE0NDYsImp0aSI6Ijl2TjJ1NnY3d2xvTlZ1T08ifQ.RvPbPft8m00RM7b7QuBGOkNqoshmITMM2dcmtoleFFg','2018-03-21 11:24:06',NULL,NULL,'2018-03-21 11:24:05','2018-03-21 11:24:06',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
