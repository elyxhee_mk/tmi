<?php

namespace TMI\Data\Repositories;

use Illuminate\Support\Facades\Event;
use TMI\Data\Models\Content;
use TMI\Data\Contracts\RepositoryContract;
//use TMI\Data\Models\Content;


use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use TMI\Support\Helper;
use Storage, Image,FFMpeg;
use \DB;

class ContentRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of Content Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    

    /**
     *
     * This is the prefix of the cache key to which the 
     * product data will be stored
     * product Auto incremented Id will be append to it
     *
     * Example: product-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'content-';
    protected $_cacheTotalKey = 'total-content';
   

    public function __construct(Content $content) {
        $this->builder = $content;
        $this->model = $content;
        
    }
    public function create(array $data = [],$details = false,$encode=true) {
        if(isset($data['content']) && $data['content'] != NULL){
        $content = $this->saveImage($data['content']);
        if($content){
            // dd($content);
            $data['path'] = $content;
            if($data['type'] == 'video'){
                $videoName = $content;
                list($thumbnailName, $ext) = explode('.', $videoName);
                // dd($videoName);
                $video = FFMpeg::open('public/files/'.config('app.files.courses.folder_name').'/'.$videoName)
                    ->getFrameFromSeconds(1)
                    ->export()
                    ->toDisk('public')
                    ->save(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                    $fileThumb = Storage::get(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                    $image = Image::make($fileThumb);
                    $width = $image->width();
                    $height = $image->height();
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@3x.png', $image->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@2x.png', $image->resize($width / 1.5, $height / 1.5)->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@thumb.png', $image->fit('510', '240', function($constraint) { $constraint->aspectRatio(); })->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png', $image->resize($width / 3, $height / 3)->stream());

            }
           
        }
        }else{
            $data['path'] = "NULL";
            $data['original_name'] = "NULL";
            //$data['type'] = "text";
            $data['file_type'] = "NULL";
        }
            
        unset($data['content']);
        //dd($data);
        parent::setEncodedKeys(array('user_id'));
        if ($course = parent::create($data,true,true)) {
            return $course;
        }
        return false;
    }
    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);
        //dd($data,$details);
        if ($data) {           
            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            if(isset($data->path) && $data->path != Null){

                if (strpos($data->path, '.') !== false) {
                    list($file, $extension) = explode('.', $data->path);
                    $file = config('app.files.courses.folder_name')."/".$file; 
                    //dd("files",$file,config('app.url')) ;                  
                    $data->image_urls['1x'] = config('app.url').'storage/files/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'storage/files/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'storage/files/'.$file.'@3x.'.$extension;
                } 
            }
            if($details && $data->content_type == "course")
            {
                //findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true)
                //dd(array('parent_id'=>$id));
                $data->modules = $this->findByAll("","",array('parent_id'=>$id));
                //dd("course module",$data->modules);
            }

        }        
        return $data;
    }

    public function hasRole($id, $role) {
        return $this->model_user_role->where('user_id', '=', $id)
                                ->where('role_id', '=', $role)
                                ->count() > 0;
    }

    private function crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.courses.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.courses.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.courses.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }
    /*will be needing this when user will be searching for store with its description*/
    public function storeSearch($pagination = false,$perPage = 10, $data = []){

        $this->builder = $this->model
        ->where(function($query) use($data) {
            $query->where('username', 'LIKE', "%{$data['keyword']}%");
            $query->orWhere('fullname', 'LIKE', "%{$data['keyword']}%");
        })
        ->where('visibility','=',1)
        ->leftJoin('user_roles',function($joins){
            $joins->on('users.id','=','user_roles.user_id');
        })->where('user_roles.role_id','=', Role::USER)
        ->select('users.id');

        return parent::findByAll($pagination, $perPage);
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true){

        //dd("find by all",$data);
       $content = $this->builder;       
       
        if( isset($data['id']) && $data['id'] != ""){
            $content = $content->where('id',$data['id']); 
        }
         if( isset($data['parent_id']) && $data['parent_id'] != ""){
            $content = $content->where('parent_id',$data['parent_id']); 
        }
        if( isset($data['user_id']) && $data['user_id'] != ""){
            $content = $content->where('user_id',$data['user_id']); 
        } 
         if( isset($data['content_type']) && $data['content_type'] != ""){
            $content = $content->where('content_type',$data['content_type']); 
        }      
        if( isset($data['filter_by_title']) && $data['filter_by_title'] != ""){
            $content = $products->whereRaw('(contents.title LIKE "%'.$data['filter_by_title'].'%")');
        }
        $this->builder = $content;
        if($data['parent_id'] == 7)
            dd($this->builder->toSql(),$this->builder->getBindings());
       $content = parent::findByAll($pagination,$perPage,[],$detail,$encode);
       //dd($content);
        if($content != NULL){
            return $content;
        }else{
            return NULL;
        }
    } 
    private function saveImage($image) {
        //dd(config('app.files.courses.folder_name'));
        $action = $image->store(config('app.files.courses.folder_name'));
        if($action){
            $crop = $this->crop($image);
        }
        return $image->hashName(); //config('app.files.courses.folder_name').'/'.$name.'.'.$ext;
    } 
    public function update(array $data = [],$details = false,$encode=true) {
        if(isset($data['content']) && $data['content'] != NULL){
        $content = $this->saveImage($data['content']);
        if($content){
            // dd($content);
            $data['path'] = $content;
            if($data['type'] == 'video'){
                $videoName = $content;
                list($thumbnailName, $ext) = explode('.', $videoName);
                // dd($videoName);
                $video = FFMpeg::open('public/files/'.config('app.files.courses.folder_name').'/'.$videoName)
                    ->getFrameFromSeconds(1)
                    ->export()
                    ->toDisk('public')
                    ->save(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                    $fileThumb = Storage::get(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png');
                    $image = Image::make($fileThumb);
                    $width = $image->width();
                    $height = $image->height();
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@3x.png', $image->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@2x.png', $image->resize($width / 1.5, $height / 1.5)->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'@thumb.png', $image->fit('510', '240', function($constraint) { $constraint->aspectRatio(); })->stream());
                    $thumbStore = Storage::put(config('app.files.courses.folder_name').'/thumbnails/'.$thumbnailName.'.png', $image->resize($width / 3, $height / 3)->stream());

            }
           
        }
        }else{
           /* $data['path'] = "NULL";
            $data['original_name'] = "NULL";
            $data['type'] = "text";
            $data['file_type'] = "NULL";*/
        }
            
        unset($data['content']);
        //dd($data);
        if ($course = parent::update($data,true,false)) {
            return $course;
        }
        return false;
    } 
    public function getCourseDetail($id, $refresh = false, $details = false, $encode = true) {
        //dd("course id".$id);
        /*
        SELECT c1.title AS course ,c2.title AS module,c3.title AS task ,c1.content_type, c1.id,c1.parent_id 
        FROM contents c1,contents c2,contents c3 WHERE c3.parent_id=c2.id AND c2.parent_id=c1.id; id=8;
        */
        $res = DB::select(DB::raw("SELECT c1.title AS course,c1.id AS course_id ,c2.title AS module,
            c2.id AS module_id,c3.title AS task ,
            c3.id AS task_id,c1.content_type, c1.id course_id,c2.parent_id module_parent_id,c3.parent_id task_parent_id
        FROM contents c1,contents c2,contents c3 WHERE c3.parent_id=c2.id AND c2.parent_id=c1.id 
        and c1.id=8"));
        dd($res);
       /* ->        
        join('contents AS c2', 'c2.parent_id', '=', 'c1.parent_id')->
        join('contents AS c3', 'c3.parent_id', '=', 'c2.parent_id')->
        where('c1.id', 8);//->get();*/
        return $res;
        //dd($res);


        /*$res = DB::table('products as t1')->
        select('t2.title')->
        join('products AS t2', 't2.color_id', '=', 't1.color_id')->
        where('t1.id', 1)->
        where('td2.id', '<>', 't1.id')->
        get();
        $this->builder = $this->builder::where()
        dd($this->builder);*/
    }
    public function getCourseDetail_backup($id, $refresh = false, $details = false, $encode = true) {
        $id =8;
        $data = parent::findById($id, $refresh, $details, $encode);
        //dd($data,$details);
        if ($data) {           
            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            if(isset($data->path) && $data->path != Null){

                if (strpos($data->path, '.') !== false) {
                    list($file, $extension) = explode('.', $data->path);
                    $file = config('app.files.courses.folder_name')."/".$file; 
                    //dd("files",$file,config('app.url')) ;                  
                    $data->image_urls['1x'] = config('app.url').'/storage/app/public/files/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'/storage/app/public/files/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'/storage/app/public/files/'.$file.'@3x.'.$extension;
                } 
            }
            if($details && $data->content_type == "course")
            {
                //findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true)
                //dd(array('parent_id'=>$id));
                $find_module['parent_id'] =$id;
                $data->modules = $this->findByAll("","",$find_module);
                //dd(count($data->modules['data']));
                if(count($data->modules['data']) > 0)
                {
                    foreach($data->modules['data'] as $key => $module)
                    {
                        //dd($module->id);
                        //dd($this->findByAll("","",array('parent_id'=>hashid_decode($module->id))));
                        //dd(hashid_decode($module->id));
                        $find["parent_id"] =hashid_decode($module->id);
                        $module->tasks = $this->findByAll("","",$find);
                        dd($module->tasks);
                    }
                       // dd($module->tasks['data']);
                }
                //dd("course module",$data->modules);
            }

        }        
        return $data;
    }             

}