<?php

namespace TMI\Data\Repositories;

use Illuminate\Support\Facades\Event;

//use TMI\Data\Models\UserEnrollment;
use TMI\Data\Models\UserEnrollment;
use TMI\Data\Contracts\RepositoryContract;
use TMI\Data\Repositories\ContentRepository;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use TMI\Support\Helper;
use Storage, Image;
use \App;

class EnrollmentRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of Content Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    

    /**
     *
     * This is the prefix of the cache key to which the 
     * product data will be stored
     * product Auto incremented Id will be append to it
     *
     * Example: product-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'enrollment-';
    protected $_cacheTotalKey = 'total-enrollment';
   

    public function __construct(UserEnrollment $enroll) {
        $this->builder = $enroll;
        $this->model = $enroll;
        $this->contentRepo = App::make('ContentRepository');
        
    }
    public function add(array $data = [],$encode=true) {
        
        //dd("addd");
        $keys = array('course_id','user_id');

        parent::setEncodedKeys($keys);
        //dd("hashing keys set",$this->keyToEncode);
        if ($enrollment = parent::create($data,true,true)) {
            return $enrollment;
        }
        return false;
    }
    public function findById($id, $refresh = false, $details = false, $encode = true) {

        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            if($details)
                $data->courseDetail = $this->contentRepo->findById(hashid_decode($data->course_id));

        }        
        return $data;
    } 

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true){

        $content = $this->builder;       
       
        if( isset($data['id']) && $data['id'] != ""){
            $content = $content->where('user_enrollments.id',$data['id']); 
        }
        if( isset($data['user_id']) && $data['user_id'] != ""){
            $content = $content->where('user_enrollments.user_id',$data['user_id']); 
        } 
         
       /* if($detail)
        {
            $content = $content->join('contents','contents.id','=','user_enrollments.course_id');
            if( isset($data['content_type']) && $data['content_type'] != ""){
            $content = $content->where('content_type',$data['content_type']); 
            }      
            if( isset($data['filter_by_title']) && $data['filter_by_title'] != ""){
                $content = $content->whereRaw('(contents.title LIKE "%'.$data['filter_by_title'].'%")');
            }
        }*/
        $this->builder = $content; 
 
        //dd($this->builder->toSql(),$this->builder->getBindings());      
        parent::setEncodedKeys(array('user_id','course_id'));
       $content = parent::findByAll($pagination,$perPage,[],$detail,$encode);
	//dd($content);
       /*if($detail)
       {
            if(count($content["data"]))
            {
                foreach($content["data"] as $key =>$course)
                {
                    $course->courseDetail = $this->contentRepo->findById(hashid_decode($course->course_id));
                }           

            }

       }*/
       
        if($content != NULL){
            return $content;
        }else{
            return NULL;
        }
    } 
    
                  

}