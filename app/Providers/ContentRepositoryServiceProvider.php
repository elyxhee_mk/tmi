<?php

namespace TMI\Providers;

use Illuminate\Support\ServiceProvider;
use TMI\Data\Repositories\ContentRepository;
use TMI\Data\Models\Content;
use TMI\Data\Models\Role;
use TMI\Data\Models\UserRole;


class ContentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ContentRepository', function () {
            return new ContentRepository(new Content);
        });
    }
}
