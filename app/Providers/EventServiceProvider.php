<?php

namespace TMI\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'TMI\Events\Event' => [
            'TMI\Listeners\EventListener',
        ],
        'TMI\Events\test' => [
            'TMI\Listeners\testconfirm',
        ],
	'TMI\Events\PasswordWasRecovered' => [
            'TMI\Listeners\PasswordWasRecoveredConfirmation',
        ],'TMI\Events\PasswordWasReset' => [
            'TMI\Listeners\PasswordWasResetConfirmation',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
