<?php

namespace TMI\Providers;

use Illuminate\Support\ServiceProvider;
use TMI\Data\Repositories\UserRepository;
use TMI\User;
use TMI\Data\Models\UserCode;
use TMI\Data\Models\Role;
use TMI\Data\Models\UserRole;
/*use konnect\Data\Models\PostAction;
use konnect\Data\Models\Post;
use konnect\Data\Models\UserSocialAccount;
use konnect\Data\Models\BusinessLocation;*/

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserRepository', function () {
            return new UserRepository(new User, new BusinessLocation , new UserSocialAccount  );
        });
    }
}
