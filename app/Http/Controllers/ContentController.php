<?php

namespace TMI\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;
use TMI\Data\Models\Content;
use TMI\Data\Repositories\ContentRepository;
use Validator;

class ContentController extends Controller
{
   const PER_PAGE = 10;
   private $_repository;

    public function __construct(ContentRepository $repo) {
        
        $this->_repository = $repo;
        $this->per_page = 10;        
    }
    public function addContent(Request $request) {

    	// phpinfo();
        // dd("aaaaa");
    	$input = $request->only('user_id','title','description','content_type','parent_id','task_content','course_category','type');
        $input['parent_id'] = hashid_decode($input['parent_id']);
        if($input['content_type'] == "course"){
             $input['content'] = $request->file('content');
        }
        else
            $input['content'] = NULL;        

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'title'             =>  'sometimes|max:100',
            'description'       =>  'sometimes|max:200',            
            'content_type'         =>  'required|in:course,module,task'
            
        ];
       // dd($input);
        if(isset($input['content_type']) && $input['content_type'] == "course")             
                $rules['course_category']  =   'required|in:Anchor,Journalist';             

        if(isset($input['parent_id']) && $input['parent_id'] != "")
        {
        	//dd("isset paresnt id");
             if(isset($input['content_type']) && $input['content_type'] == "module")        	 	
        	 	$rules['parent_id']  =   'required|exists:contents,id,content_type,course';
        	 else if(isset($input['content_type']) && $input['content_type'] == "task")
        	 {        	 	
        	 	$rules['parent_id']  =   'required|exists:contents,id,content_type,module';
        	 	$rules['task_content']  =   'required|max:2500';
			$rules['type']  =   'required|in:text,video,audio';
        	 }
        	 else
        	 	unset($input["parent_id"]);
        }           	 	

        if($input['content'] != NULL){
            $rules['content']  =   'sometimes|mimetypes:image/png,image/jpg,image/jpeg,image/gif,video/avi,video/mp4,video/avi';
        }
        $validator = Validator::make($input, $rules);        
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	//dd($input);
            if($input['content'] != NULL){
                //dd($input['content']);
                $mime = $request->file('content')->getMimeType();
                $extension = $request->file('content')->getClientOriginalExtension();
                if($mime == 'image/png' || $mime == 'image/jpg'|| $mime == 'image/jpeg'||$mime == 'image/gif'){
                    $input['type'] = 'photo';
                }else{
                    $input['type'] = 'video';
                }
                $input['file_type'] = $mime;
                $input['original_name'] = $request->file('content')->getClientOriginalName();
            }
		
		//dd($input);            
            $response = $this->_repository->create($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function updateContent(Request $request) {

    	 
    	$input = $request->only('title','user_id','description','content_type','parent_id','task_content','content_id');
        
        $input['content'] = $request->file('content');
        $input['id'] = $input['content_id'];

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'title'             =>  'sometimes|max:100',
            'description'       =>  'sometimes|max:200',            
            'content_type'         =>  'required|in:course,module,task'
            
        ];
        if(isset($input['parent_id']) && $input['parent_id'] != "")
        {
        	 if(isset($input['content_type']) && $input['content_type'] == "module")        	 	
        	 	$rules['parent_id']  =   'required|exists:contents,id,content_type,course';
        	 else if(isset($input['content_type']) && $input['content_type'] == "task")
        	 {        	 	
        	 	$rules['parent_id']  =   'required|exists:contents,id,content_type,module';
        	 	$rules['task_content']  =   'required|max:1500';
        	 }
        	 else
        	 	unset($input["parent_id"]);
        }
        if(isset($input['id']) && $input['id'] != "")
        {
        	$rules['id']  =   'required|exists:contents,id,deleted_at,NULL';
        }

       	/*if(isset($input['content_type']) && ($input['content_type'] == "module" 
       		|| $input['content_type'] == "task")) 
       		$rules['parent_id']         =  'required|exists:contents,id,deleted_at,NULL'; */      	 	

        if($input['content'] != NULL){
            $rules['content']  =   'sometimes|mimetypes:image/png,image/jpg,image/jpeg,image/gif,video/avi,video/mp4,video/avi';
        }
        
        
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	//dd($input);
        	unset($input['user_id']);
        	unset($input['content_id']);
            if($input['content'] != NULL){
                $mime = $request->file('content')->getMimeType();
                $extension = $request->file('content')->getClientOriginalExtension();
                if($mime == 'image/png' || $mime == 'image/jpg'|| $mime == 'image/jpeg'||$mime == 'image/gif'){
                    $input['type'] = 'photo';
                }else{
                    $input['type'] = 'video';
                }
                $input['file_type'] = $mime;
                $input['original_name'] = $request->file('content')->getClientOriginalName();
            }            
            $response = $this->_repository->update($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }

    public function all(Request $request)
    {
    	$input = $request->only('user_id','id','keyword','content_type','parent_id');
    	//dd($input);
	
    	if(isset($input['id']) && $input['id'] != "")
    	{
        	$input['id'] = hashid_decode($input['id']);
		$rules['id'] = 'required|exists:contents,id,deleted_at,NULL';        	
    	}
    	if(isset($input['content_type']) && $input['content_type'] != "")
    	{
        	$rules['content_type'] = 'required|exists:contents,id,deleted_at,NULL';        	
    	}
    	$rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'keyword'             =>  'sometimes|max:10'
        ];
        if(isset($input['id']) && $input['id'] != "")
        	$rules['id'] = 'required|exists:contents,id,deleted_at,NULL';

        $validator = Validator::make($input, $rules);
       
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	      
            if(isset($input['id']) && $input['id'] != ""){
            	$response = $this->_repository->findById($input['id']);
            }
            else{
            	$response = $this->_repository->findByAll(true,$this->per_page,$input);
            }
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function getDetail(Request $request)
    {
        $input = $request->only('user_id','course_id');
        $input['course_id'] = hashid_decode($input['course_id']);
       

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'course_id'             =>  'required|exists:contents,id,deleted_at,NULL',
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            //dd($input);
            
            $response = $this->_repository->getCourseDetail($input['course_id'],"",true);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);

    }
        
    
}
