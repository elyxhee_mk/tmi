<?php

namespace TMI\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;
use TMI\Data\Models\UserEnrollment;
use TMI\Data\Repositories\EnrollmentRepository;
use Validator;

class EnrollController extends Controller
{
    private $_repository;

    public function __construct(EnrollmentRepository $repo) {
        
        $this->_repository = $repo;
        $this->per_page = 10;        
    }
    public function add(Request $request)
    {
    	 
    	$input = $request->only('user_id','course_id');
        $input['course_id'] = hashid_decode($input['course_id']);
        

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',                     
            'course_id'         =>  'required|exists:contents,id,content_type,course,deleted_at,NULL'            
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            $enrolled =UserEnrollment::where('course_id',$input['course_id'])->where('user_id',$input['user_id'])->count();
            if($enrolled > 0)
             {
                $code = 200;
                $output = ['error' => [ 'code' => $code, 'messages' => 'User is Already Enrolled in this Course' ] ];
            }
        	else 
            {                 
             $response = $this->_repository->add($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
            }
        }

        return response()->json($output, $code);
    }
    public function userEnrolledCourses(Request $request)
    {
    	$input = $request->only('user_id','details');
        
        

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'details'             =>  'required|in:true,false'
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	
            //dd($input);     
            $response = $this->_repository->findByAll(true,$this->per_page,$input,$input["details"]);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);	
    }
}
