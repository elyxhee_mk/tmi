<?php

namespace TMI\Http\Controllers;

use Illuminate\Http\Request as localRequest;
use \GuzzleHttp\Client;
use \GuzzleHttp\Psr7;
use \GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class EventController extends Controller
{
    //fetch list of events from third party
    private $base_url;
    public function __construct()
    {
    	$this->base_url = "http://urbangateways.org/wp-json/tribe/events/v1/";
    }
    public function all(localRequest $localRequest){
    	//dd("list event");
    	//tags,categories,venue,organizer => array();
    	/*http://urbangateways.org/wp-json/tribe/events/v1/events?page=1&per_page=10&start_date=2018-01-09%2017%3A59%3A00&end_date=2020-01-10%2011%3A09%3A21&search=test&tags=1,2&venue=2,3&organizer=3,5&featured=false&status=test&geoloc=true&geoloc_lat=12&geoloc_lng=34*/
    	$key_arrays_filter = array('tags','categories','venue','organizer');

    	$input = $localRequest->only('page','per_page','start_date','end_date','search','categories','tags','venue','organizer','featured','status','geoloc','geoloc_lat','geoloc_lng');
    	$queryString = "?";
    	foreach($input as $filter => $value)
    	{
    		$queryString .= $filter."=".$value."&";
    	}
    	$queryString = rtrim($queryString,"&");
    	//dd("input",$input,"queryString",$queryString);
    	//dd($this->base_url);
    	$client = new Client(['base_uri' => $this->base_url]);
    	$uri = "events";
    	if(trim($queryString) != "")
    		$uri .= $queryString;
    	
    	//$client = new Client(['base_uri' => "http://localhost/TeenArtPass/public/api/"]);
    	//
    	/*$response = $client->get('http://localhost/TeenArtPass/public/api/preload/data');
    	dd("response from http bin",$response->getStatusCode(),$response->json());
    	
    	$response = $client->get('http://urbangateways.org/wp-json/tribe/events/v1/events?id=1');
    	dd("response",$response);	*/	
    	try{
    		$response = $client->request('GET', $uri);			
		}
		catch (RequestException $e){
		    //dd(json_decode($e->getRequest()->getBody()));
		    //dd("Request exception",Psr7\str($e->getRequest()));
		    if ($e->hasResponse()) {
		    	
		    	//dd(getallheaders());
		    	$response = $e->getResponse();
		    	//dd("it has response"); 
		        //dd("has respones",Psr7\str($e->getResponse()));
		    }
		}
		//return $response;
		$code = $response->getStatusCode(); // 200
		$data = json_decode($response->getBody());

		$output["code"] = $code;
		$output["data"] = $data->data;
		return $output;
		/*
		$body = $client->get(....)->getBody();

			$obj = json_decode($body);
		*/
		dd("code",$code,"data",$data->data);

		//$code = $response->getStatusCode(); // 200
		//$reason = $response->getReasonPhrase();
		//dd("response is",$code,$reason);
		
    }
    public function a()
    {

    	dd("aaaa");
    }
}
