<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('auth/register', 'UserController@register');
Route::post('/', 'UserController@register');
Route::post('auth/login', 'UserController@login');
Route::post('auth/logout', 'UserController@logout');*/


//----------USER MODULE----------------
Route::post('/register', ['uses'=>'UserController@register']);
Route::post('/login', ['uses'=>'UserController@login']);
Route::put('/forgot-password',['as'=>'user.forgot-password','uses'=> 'UserController@forgotPassword']);
Route::put('/reset-password',['as'=>'user.reset-password','uses' => 'UserController@resetPassword']);
Route::post('/user/profile/view', ['uses'=>'UserController@view']);
Route::post('/user/profile/upload', ['uses'=>'UserController@upload']);
Route::get('/user/list', ['uses'=>'UserController@all']);
Route::post('/user/visibility', ['uses'=>'UserController@setVisibility']);
Route::post('/user/profile/update', ['uses'=>'UserController@update']);
Route::get('/user/token/refresh', ['uses'=>'UserController@refreshToken']);
Route::post('/device-token/set', ['uses'=>'DeviceController@setUserTokens']);
Route::post('/device-token/remove', ['uses'=>'DeviceController@removeUserTokens']);
Route::post('/add/friend', ['uses'=>'UserController@addFriend']);
Route::post('/update/friend/status', ['uses'=>'UserController@updateFriend']);
Route::get('/friends/list', ['uses'=>'UserController@friendList']);
Route::post('/add/user/detail', ['uses'=>'UserDetailController@addUserDetail']);
Route::get('/user/detail/list', ['uses'=>'UserDetailController@all']);
Route::get('/mutual/connection/list', ['uses'=>'UserController@listMutualFriends']);
Route::get('/mutual/tags/list', ['uses'=>'UserController@listMutualTags']);
Route::post('/user/add/view', ['uses'=>'UserController@addUserView']);
Route::post('/add/dream/collage', ['uses'=>'UserController@upload']);
Route::post('/refer/friend', ['uses'=>'UserController@referFriend']);
Route::post('/update/user/detail', ['uses'=>'UserDetailController@updateUserDetail']);
Route::get('/user/mutual/hashtags', ['uses'=>'UserDetailController@userMutualHashTag']);

//User Search
Route::get('/user/search',['as'=>'user.search','uses'=> 'UserController@userSearch']);
//--------------------------------------

Route::group(['middleware' => array('jwt.auth','validaterole')], function () {
	//Route::get('/preload/data', 'PreLoadData@get');
    Route::get('user', 'UserController@getAuthUser');
    Route::post('/content/create', ['uses'=>'ContentController@addContent']);
    Route::post('/content/update', ['uses'=>'ContentController@updateContent']);
    //Route::post('/user/enroll', 'EnrollController@add');
});
Route::group(['middleware' => 'jwt.auth'], function () {
	//Route::get('/preload/data', 'PreLoadData@get');
    Route::get('/content/list', 'ContentController@all'); 
    Route::post('/user/enroll', 'EnrollController@add');   
    Route::get('/user/courses', 'EnrollController@userEnrolledCourses');
    //Route::get('/course/detail', 'ContentController@getDetail'); 
    
});


